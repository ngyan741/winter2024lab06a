public class GameManager{
	
	//establish fields
	private Deck drawPile;
	private Card centerCard;
	private Card playerCard;
	
	//constructor that intializes drawPile as a new Deck object and shuffles it, then draws the top 2 cards of drawPile and intializes those to centerCard and playerCard
	public GameManager(){
		this.drawPile = new Deck();
		this.drawPile.shuffle();
		this.centerCard = this.drawPile.drawTopCard();
		this.playerCard = this.drawPile.drawTopCard();
	}
	
	//getter
	public Deck getDrawPile(){
		return this.drawPile;
	}
	
	//toString method that returns the current value of the center card and the player card
	public String toString(){
		return "--------------------------------" + "\n" + "Center card: " + this.centerCard + "\n" + "Player card: " + this.playerCard + "\n" + "--------------------------------";
	}
	
	//method that shuffles the deck, draws a card and sets the value of the centerCard to it. Does the same for the playerCard.
	public void dealCards(){
		this.drawPile.shuffle();
		this.centerCard = this.drawPile.drawTopCard();
		this.drawPile.shuffle();
		this.playerCard = this.drawPile.drawTopCard();
	}
	
	//method that returns the number of cards in drawPile
	public int getNumberOfCards(){
		return this.drawPile.length();
	}
	
	//method that calculates the player's points. If the centerCard and playerCard have the same value, it returns 4, if they have the same suit, it returns 2, and if they have neither, it returns -1.
	public int calculatePoints(){
		if(this.centerCard.getValue().equals(this.playerCard.getValue())){
			return 4;
		}
		if(this.centerCard.getSuit().equals(this.playerCard.getSuit())){
			return 2;
		}
		else{
			return -1;
		}
	}
}