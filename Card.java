public class Card{
	//establish suit and value fields
	private String suit;
	private String value;
	
	//constructor that takes in two String inputs 
	public Card(String suit, String value){
		this.suit = suit;
		this.value = value;
	}
	
	//getters
	public String getSuit(){
		return this.suit;
	}
	
	public String getValue(){
		return this.value;
	}
	
	//toString method
	public String toString(){
		return this.value + " of " + this.suit;
	}
}