public class LuckyCardGameApp{
	public static void main (String args[]){
		//create new GameManager object called manager
		GameManager manager = new GameManager();
		//create new int variables to track points/round count
		int totalPoints = 0;
		int roundCounter = 1;
		//greet the player
		System.out.println("Welcome Player to Lucky!");
		//main game loop: continues to run if the drawpile has more than 1 card AND total points is less than 5. It prints the current round, then the center card and the player card, calculates the points and sets it to the total points, then prints the total points. It then deals new cards and the round counter increases.
		while(manager.getDrawPile().length() > 1 && totalPoints < 5){
			System.out.println("Round: " + roundCounter);
			System.out.println(manager);
			totalPoints = totalPoints + manager.calculatePoints();
			System.out.println("Your points after round " + roundCounter + ": "+ totalPoints+ "\n" + ".................................................................");
			manager.dealCards();
			roundCounter++;
		}
		//after the game has ended, prints the player's final score
		System.out.println("Final Score: " + totalPoints);
		//if they had more than 5 points, they won!
		if(totalPoints < 5){
			System.out.println("You Lose...");
		}
		//if they couldnt get 5 points before the drawpile ran out of cards, they lost.
		else{
			System.out.println("You Win! Congratulations.");
		}
	}
}